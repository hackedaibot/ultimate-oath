const Deferred = require("../src")

const t = new Deferred("Да так и промисы умеют!", 3000)
t.then(value  => { console.log(value) })
  .catch(error => { console.log(error) })
t.resolve("Че тут такого")

const t0 = new Deferred("Так падажжи ебана...", 3000)
  .then(value  => { console.log(value) })
  .catch(error => { console.log(error) })
  .resolve("...какого черта резолв попал на промис?")

const t1 = new Deferred("Великоскрепные ожидания что не сломается 1", 3000)
  .then(value  => { console.log(value) })
  .catch(error => { console.log(error) })
  .resolve("не сломалось 1")

const t2 = new Deferred("Великоскрепные ожидания что не сломается 2", 3000)
  .resolve("не сломалось 2")
  .then(value  => { console.log(value) })
  .catch(error => { console.log(error) })

const t3 = new Deferred("Великоскрепные ожидания что не сломается 3", 3000)
  .then(value  => { console.log(value) })
  .resolve("не сломалось 3")
  .catch(error => { console.log(error) })

// [Running] ts-node "ProgrammingResearch/utils/Deferred.ts"
// ==============================================================
// Че тут такого
// ...какого черта резолв попал на промис?
// не сломалось 1
// не сломалось 2
// не сломалось 3
// ==============================================================
// HOW?
// ==============================================================


