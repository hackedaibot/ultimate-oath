# Ultimate Oath

It is like taking tons of Promises and redoing their internals through vivisection without anesthesia.


```ts
const Deferred = require("../src")

const t = new Deferred("Да так и промисы умеют!", 3000)
t.then(value  => { console.log(value) })
  .catch(error => { console.log(error) })
t.resolve("Че тут такого")

const t0 = new Deferred("Так падажжи ебана...", 3000)
  .then(value  => { console.log(value) })
  .catch(error => { console.log(error) })
  .resolve("...какого черта резолв попал на промис?")

const t1 = new Deferred("Великоскрепные ожидания что не сломается 1", 3000)
  
t1.then(value  => { console.log(value) })
  .catch(error => { console.log(error) })
  .resolve("не сломалось 1")

const t2 = new Deferred("Великоскрепные ожидания что не сломается 2", 3000)
  .resolve("не сломалось 2")
  .then(value  => { console.log(value) })
  .catch(error => { console.log(error) })

const t3 = new Deferred("Великоскрепные ожидания что не сломается 3", 3000)
  .then(value  => { console.log(value) })
  .resolve("не сломалось 3")
  .catch(error => { console.log(error) })

// [Running] ts-node "ProgrammingResearch/utils/Deferred.ts"
// ==============================================================
// Че тут такого
// ...какого черта резолв попал на промис?
// не сломалось 1
// не сломалось 2
// не сломалось 3
// ==============================================================
// HOW?
// ==============================================================
```

Список примененного хардкора:

1. resolve и reject можно вызывать синхронно до того как коллбэк 
   конструктора промиса имеет возможность исполниться

2. Методы resolve и reject передаются по цепочке .then/.catch
   и не нарушают логику цепочки (короче как tap --- никак не меняя)

3. Таймаут по умолчанию. После истечения вызывается reject 
   с указанием этой причины.

4. Аккуратная внутренняя реализация использующая деклоаторы функций
   для получения надежных гарантий касательно исполнения.

```ts
   /**
    * Здесь мы потрошим самые сакральные обещания
    * @see https://www.youtube.com/watch?v=5HH-MqsB8L4
    */
   
   function wrapMethod(promise, methods) {
   	const originalcatch = promise.catch;
   	const originalthen  = promise.then;
   	
   	promise.catch = function () {
   	  const result = originalcatch.apply(promise, arguments)
   	  return wrapMethod(result, methods)
	}   
	
	promise.then = function () {
	  const result = originalthen.apply(promise, arguments)
	  return wrapMethod(result, methods)
	}
        ...
        
        return promise // Что вы со мной сделали, уроды? =(
   }
```

5. Расширяемость? Сделать очередь промисов? Легко. Возможность отмены
   асинхронной операции -- как два промиса реджекнуть. 
   Авторетрай по кастомному .retry({ ... })? Два метода припаяли,
   и с этим проблем не будет.

На этом все.
