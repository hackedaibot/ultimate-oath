/**
 * @class {Духовные скрепы}
 * 
 * @param name    имя чтобы легче было понять, че за скрепа
 * @param timeout определяет сколько выдержит духовность без резолва
 */
export function Deferred(name: string,  timeout: number = 3000) {
    var self = this, to: NodeJS.Timeout = null;

  const promise = new Promise((resolve, reject) => {
	to = setTimeout(onTimeout, timeout)
    this._resolve = stopTimerAnd(resolve);
    this._reject  = stopTimerAnd(reject);
  });

  this.promise = wrapMethod(promise, {
	  // This is required to be able resolve the value sync
	  // Not breakin a promise chain in any way
  	  resolve: value => setImmediate(self._resolve, value), 
	  reject : value => setImmediate(self._reject , value) 
  })

  this.catch   = this.promise.catch
  this.then    = this.promise.then
  this.resolve = this.promise.resolve
  this.reject  = this.promise.reject

  function stopTimerAnd(fn) {
	 return function () {
		 clearTimeout(to)
		 return fn.apply(this, arguments)
	 }
  }

  function onTimeout() {
	const error = new Error('Deferred timed out: ' + name)
	define(error, "code", 'ETIMEDOUT')
	define(error, "info", { timeout, name })
	self.reject(error)
  }
}

/**
 * Здесь мы потрошим самые сакральные обещания
 * @see https://www.youtube.com/watch?v=5HH-MqsB8L4
 */

function wrapMethod(promise, methods) {
	const originalcatch = promise.catch;
	const originalthen  = promise.then;
	
	promise.catch = function () {
	  const result = originalcatch.apply(promise, arguments)
	  return wrapMethod(result, methods)
	}
	
	promise.then = function () {
	  const result = originalthen.apply(promise, arguments)
	  return wrapMethod(result, methods)
	}

	promise.resolve = function () {
	  methods.resolve.apply(null, arguments)
	  return this;
	}

	promise.reject = function () {
	  methods.reject.apply(null, arguments)
	  return this;
	}

	return promise
}

/**
 * Helper function (like define-property on npm)
 */
function define(object, property, value) {
	Object.defineProperty(object, property, { value })
}

// Alias
export const UltimateOath = Deferred;
export default UltimateOath

